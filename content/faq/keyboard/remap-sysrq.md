+++
title = "My laptop has no SysRq key. Can I remap any key to SysRq?"
+++

Yes! Similar to remapping the Arrow-Keys above, you can remap - say the `menu (fn+RightCtrl)` key to `SysRq`.  
Just add another line to `/etc/udev/hwdb.d/90-nkey.hwdb` with the following, including the leading whitespaces:
```
  KEYBOARD_KEY_<ScanCode>=sysrq        # force remap sysrq to Fn+RightCtrl
```
You can get the `<ScanCode>` by running
```
evtest /dev/input/by-id/usb-ASUSTeK_Computer_Inc._N-KEY_Device-*-kbd
```
and pressing the `RightCtrl` key.  
In this case, it is `70065`
```
Testing ... (interrupt to exit)
Event: time 1662839073.640933, type 4 (EV_MSC), code 4 (MSC_SCAN), value 70065 <--------- Substitute this as <ScanCode>
Event: time 1662839073.640933, type 1 (EV_KEY), code 127 (KEY_COMPOSE), value 1
Event: time 1662839073.640933, -------------- SYN_REPORT ------------
```
Then update hwdb with:

```
sudo systemd-hwdb update
sudo udevadm trigger